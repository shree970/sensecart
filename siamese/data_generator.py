"""
This script can be used to automatically crop the products for generating
train data
"""

import numpy as np
import os
import tensorflow as tf
import cv2
import sys
import argparse

# directory containing the images
ap = argparse.ArgumentParser()
ap.add_argument('-d','--dir', required=True, type=str)
args = ap.parse_args()

# ADD PATH TO FROZEN GRAPH
PATH_TO_FROZEN_GRAPH = 'detector_model/frozen_inference_graph.pb'

# reads the frozen graph
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_FROZEN_GRAPH, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

ctr = 0

# Detection
with detection_graph.as_default():
    with tf.Session(graph=detection_graph) as sess:
        for img in os.listdir(args.dir):
            img_path = os.path.join(args.dir,img)
            # Read frame image from folder
            image_np = cv2.imread(img_path)
            # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
            image_np_expanded = np.expand_dims(image_np, axis=0)
            # Extract image tensor
            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            # Extract detection boxes
            boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            # Extract detection scores
            scores = detection_graph.get_tensor_by_name('detection_scores:0')
            # Extract detection classes
            classes = detection_graph.get_tensor_by_name('detection_classes:0')
            # Extract number of detections
            num_detections = detection_graph.get_tensor_by_name(
                'num_detections:0')
            # Actual detection.
            (boxes, scores, classes, num_detections) = sess.run(
                [boxes, scores, classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})

            w = image_np.shape[1]
            h = image_np.shape[0]

            # Visualization of the results of a detection.
            scores = np.squeeze(scores)

            objects = []
            for i, box in enumerate(np.squeeze(boxes)):
                ymin, xmin, ymax, xmax = box
                if scores[i] > 0.8:
                    # print(scores[i])
                    # cv2.rectangle(image_np, (int(xmin*w), int(ymin*h)), (int(xmax*w), int(ymax*h)), (0,0,255), 5)
                    objects.append(image_np[(int(ymin*h)+int(h*0.1)):(int(ymax*h)-int(h*0.1)),
                                            (int(xmin*w)+int(w*0.1)):(int(xmax*w)-int(w*0.1))])

            for i in range(len(objects)):
                file = '{}.jpg'.format(ctr)
                cv2.imwrite(os.path.join(args.dir, file), objects[i])
                print('written')
                ctr += 1

        sys.exit()
