"""
This is the siamese network module containing classes and functions for
loading dataset, training the model and making predictions
"""


import numpy as np
import os
from itertools import combinations, product
import random
from keras.models import Sequential, Model, load_model
from keras.preprocessing import image
from keras.applications import VGG16, MobileNetV2
# from keras.applications.vgg16 import preprocess_input
from keras.applications.mobilenet_v2 import preprocess_input
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Lambda, Input, Dropout
from keras import backend as K
import pandas as pd
import cv2

# download and load the feature extractor
FEATURE_EXTRACTOR = MobileNetV2(weights='imagenet', include_top=False, input_tensor=Input(shape=(150, 150, 3)))
FEATURE_EXTRACTOR.trainable = False

class DataLoader():

    '''Load and generate pairs of images from directory'''

    def __init__(self, data_dir, size=(150,150)):

        # initialize the variables
        self.data_dict = {}
        self.x_pos = []
        self.x_neg = []
        self.y_pos = []
        self.y_neg = []

        # read the images from data directory
        for cls in os.listdir(data_dir):
            images = []
            for img in os.listdir(os.path.join(data_dir, cls)):
                img_path = os.path.join(data_dir,cls,img)
                img_np = image.load_img(img_path, target_size=size)
                x = image.img_to_array(img_np)
                x = preprocess_input(x)
                images.append(x)
            self.data_dict[cls] = images

    def _generate_X(self):
        # generate pairs of images from same class i.e. positives
        for cls, images in self.data_dict.items():
            for pos_pair in combinations(images, 2):
                self.x_pos.append(list(pos_pair))

        # generate pairs of images from different class i.e. negatives
        for cls_pair in combinations(list(self.data_dict.values()),2):
            for neg_pair in product(cls_pair[0], cls_pair[1]):
                self.x_neg.append(list(neg_pair))

        # balance the negative examples with positive pairs
        self.x_neg = random.sample(self.x_neg, len(self.x_pos))

    def _generate_Y(self):
        # generate y labels
        self.y_pos = list(np.ones(len(self.x_pos)))
        self.y_neg = list(np.zeros(len(self.x_neg)))

    def get_data(self):
        # generate x and y
        self._generate_X()
        self._generate_Y()

        # create positive samples
        pos_samples = pd.concat([pd.DataFrame(self.x_pos), pd.DataFrame(self.y_pos)], axis=1, ignore_index=True)

        # create negative samples
        neg_samples = pd.concat([pd.DataFrame(self.x_neg), pd.DataFrame(self.y_neg)], axis=1, ignore_index=True)

        # concatenate positive and negative samples and shuffle them
        samples = pd.concat([pd.DataFrame(pos_samples), pd.DataFrame(neg_samples)], axis=0, ignore_index=True)
        samples = samples.sample(frac=1).reset_index(drop=True)

        # sample as matrix
        X_train = samples.as_matrix()[:,:-1]
        y_train = samples.as_matrix()[:,-1]

        # separate the two inputs
        x1_train = X_train[:,0]
        x2_train = X_train[:,1]

        # reshape as (n_samples, height, width, channels)
        x1_train = np.stack(x1_train)
        x2_train = np.stack(x2_train)

        return x1_train, x2_train, y_train

def siamese_model(input_dim):
    """Returns the siamese model
    """
    # the two inputs
    input_1 = Input(input_dim)
    input_2 = Input(input_dim)

    # feature extractor
    model = Sequential()
    model.add(FEATURE_EXTRACTOR)
    model.add(Flatten())
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2048, activation='relu'))
    model.add(Dense(1024, activation='sigmoid'))

    # final encoded values of images
    encoded_1 = model(input_1)
    encoded_2 = model(input_2)

    # euclidean distances
    L1_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
    L1_distance = L1_layer([encoded_1, encoded_2])

    # sigmoid layer
    prediction = Dense(1, activation='sigmoid')(L1_distance)

    return Model(inputs=[input_1, input_2], outputs=prediction)

def load_db(db_path):
    '''Load and preprocess the database images'''
    ref_db = []

    for ref in os.listdir('db'):
        img = image.load_img(os.path.join('db', ref), target_size=(150, 150))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x = preprocess_input(x)
        ref_db.append(x)

    return ref_db

def predict_file(img_path, model, db):
    '''Make prediction from an image file against the database'''
    img = cv2.imread(img_path)
    w = img.shape[1]
    h = img.shape[0]
    img = img[int(h*0.15):h-int(h*0.15), int(w*0.15):w-int(w*0.15)]

    x = cv2.resize(img, (150,150))
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    probs = []

    for ref in db:
        probs.append(model.predict([x, ref]))

    return probs, np.argmax(probs)

def predict(img, model, db):
    '''Make prediction from numpy array of image against the database'''
    w = img.shape[1]
    h = img.shape[0]
    img = img[int(h*0.15):h-int(h*0.15), int(w*0.15):w-int(w*0.15)]

    x = cv2.resize(img, (150,150))
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    probs = []

    for ref in db:
        probs.append(model.predict([x, ref]))

    return probs, np.argmax(probs)
