# Siamese Product Recognition

This directory contains the scripts, modules, data and notebooks for training
and using a siamese network for product recognition.

Download the trained model [here](https://doc-08-6c-docs.googleusercontent.com/docs/securesc/ha0ro937gcuc7l7deffksulhg5h7mbp1/kstlt73hev633el1nge4b2ot07ajmjrk/1578852000000/04132555796249906322/*/15HG5hc4wnGkourOGYCnszXWX5EDSrd7i?e=download)

### Requirements

- Tensorflow 1.15
- Keras
- OpenCV

### Contents

- [`data_generator.py`](data_generator.py): This script is used to crop products
  in images automatically for generating a dataset
- [`siamese_inference.py`](siamese_inference.py): This script is for inference on
  a single image against the [database](db/) through the CLI
- [`siamese_inference_webcam.py`](siamese_inference_webcam.py): This is for
  inference in realtime
- [`Siamese_Training.ipynb`](Siamese_Training.ipynb): This notebook is for training
  the model from scratch
- [`siamese.py`](siamese.py): This is the main siamese module containing classes
  and functions for processing data, training and inference.
- [test](test/): This directory contains some test images
- [DB](db/): This directory contains the database for use in inference
- [train_data](train_data/): This directory contains the original image dataset

### Improvements / TO-DO

- Use a feature extractor trained for classification on a food/grocery dataset
