"""
This script is for running product recognition against a database on a single
image
"""

import keras
from siamese import predict, load_db
from keras.applications.vgg16 import preprocess_input
from keras.models import load_model
import numpy as np
import cv2
import argparse

# path to image
ap = argparse.ArgumentParser()
ap.add_argument('-i','--image', required=True, type=str, help="Path to the image")
ap.add_argument('-d','--database', default='db', type=str, help="Path to the database of images to check against")
ap.add_argument('-m','--model', default='siamese.h5', type=str, help="Path to the siamese model")
args = ap.parse_args()

# load model and reference database
model = load_model(args.model)
db = load_db(args.database)

# product mapping from number
product_map = {0: 'Cherries', 1: 'Paprika', 2: 'Makhana 1', 3: 'Makhana 2', 4: 'Oregano', 5: 'Knorr Soup', -1: ''}

# make prediction
image = cv2.imread(args.image)
image = cv2.resize(image,(640, 480))
probs, pred = predict(image, model, db)
probs = np.squeeze(probs)

res = pred if probs[pred] >= 0.5 else -1

# display result
print(product_map[res])
cv2.putText(image, "Recognized: {}".format(product_map[res]), (20,30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)

cv2.imshow('Recognition', image)
print("Press Q to quit")

cv2.waitKey(0)
cv2.destroyAllWindows()
