"""
This script is for running product recognition against a database of reference
images in realtime
"""

import keras
from siamese import predict, load_db
from keras.applications.vgg16 import preprocess_input
from keras.models import load_model
import numpy as np
import cv2

def mode(List):
    """Returns most frequent element from list
    """
    return max(set(List), key = List.count)

# start camera
# cam = cv2.VideoCapture('http://192.168.1.2:8080/video')
cam = cv2.VideoCapture(0)

# load model and reference database
model = load_model('siamese.h5')
db = load_db('db')

# product mapping from number
product_map = {0: 'Cherries', 1: 'Paprika', 2: 'Makhana 1', 3: 'Makhana 2', 4: 'Oregano', 5: 'Knorr Soup', -1: ''}

ctr = 0
k_preds = []
history = [-1]
while True:
    ctr += 1
    # read frames and predict
    ret, frame = cam.read()
    # frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE) # for vertical orientation of phone
    probs, pred = predict(frame, model, db)

    print(np.squeeze(probs), pred)

    res = pred if probs[pred] >= 0.5 else -1
    k_preds.append(res)
    # get the top prediction every 5 frames
    if ctr % 5 == 0:
        res = mode(k_preds)
        history.append(res)
        k_preds = []

    res = history[-1]
    # display text on window
    cv2.putText(frame, "Recognized: {}".format(product_map[res]), (20,100), cv2.FONT_HERSHEY_SIMPLEX, 2, (0,0,255), 5)

    cv2.imshow('Recognition', cv2.resize(frame, (500, 600)))

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
cam.release()
