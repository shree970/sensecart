import requests
import cv2
from gpiozero import LineSensor
from time import sleep
import json

# Define the IR Sensor
ir1 = LineSensor(14)
ir2 = LineSensor(4)

# Define the API URL
# addr = 'http://192.168.1.4:5000'
# URl = addr + '/dev'
URL = "https://us-central1-lucid-messenger-251717.cloudfunctions.net/Sensecart-Inference"

def send_data(frame, action):

    cv2.imwrite('tmp.jpg',frame)

    file = {
        'json': json.dumps(action),
        'file': ('image', open('tmp.jpg', 'rb'))
    }
    # Send the POST request with the image file in bytes
    response = requests.post(URL, files=file)
    # Return response
    return response.text

# Start the camera
cap = cv2.VideoCapture(0)

state = 0
while(True):

    ret, frame = cap.read()

    #cv2.imshow('frame',frame)

    # Calculating the direction of product swing
    if ir1.value == 0:
        # Detected as Add gesture
        if state == 0:
            print('Adding Item')
            action = {'action': 'ADD'}
            # getting the prediction
            pred = send_data(frame, action)

            print(action['action'], pred)
            state = 0

        # Detected as Remove gesture
        elif state == 1:
            print('Removing Item')
            action = {'action': 'DEL'}
            # getting the prediction
            pred = send_data(frame, action)

            print(action['action'], pred)
            state = 0

        sleep(2)
    elif ir2.value == 0:
        state = 1

cap.release()
cv2.destroyAllWindows()
