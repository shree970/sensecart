# Cloud API for Inference

Google Vision API is used for Visual Product Search using Google Cloud Functions as the Serverless Compute Instance

### API URL

The API can be accessed by making HTTP requests in file format to the following URL:

https://us-central1-lucid-messenger-251717.cloudfunctions.net/Sensecart-Inference

### Example Usage of API

```python
import requests
import json

# API URL
URL = "https://us-central1-lucid-messenger-251717.cloudfunctions.net/Sensecart-Inference"

# meta data about action to perform on product i.e. Add to Cart or Remove from Cart
action = {'action': 'ADD'} # or {'action': 'DEL'}

# payload the image file and json
file = {
    'json': json.dumps(action),
    'file': ('image', open('image.jpg', 'rb'))
}
# Send the POST request with the image file in bytes
response = requests.post(URL, files=file)
print(response.text)
```
