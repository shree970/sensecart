import numpy as np
from vision import vps_predict
import cv2
from flask import jsonify
from firebase import firebase
import requests
import json

# get firebase realtime database
firestore = firebase.FirebaseApplication('https://sensecart-6ece7.firebaseio.com/', None)

# Session id
session_id = "SC001"

# Define the product set ID
PRODUCT_SET_ID = 'demo0'

def infer(image):
    pred, confidence = vps_predict(PRODUCT_SET_ID, image)
    if confidence > 0.5:
        return pred, confidence
    else:
        return 'Null', confidence

# Function to fetch product meta data
def generateMeta(output, quantity):
    # Fetch the product meta data
    meta = firestore.get('/products/', '')
    price = 0
    # Get only the current product's meta
    for prod in list(meta.keys()):
        if output.lower() == prod.lower():
            price = meta[prod]['price']
    # Create a dictionary of the meta
    metaData = { "quantity": quantity , "price": price }
    return metaData

# Function to add a new item to DB
def addToRTDB(session_id, product, metaData):
    # Updating the database with new item in same session
    for k, v in zip(list(metaData.keys()), list(metaData.values())):
        firestore.put('/session/{}/{}'.format(session_id, product), k, v)
    print("Put Complete")

# Function to update the quantity of an existing item in cart
def updateQuantity(session_id, product, quantity):
    # Updating the quantity of a product in cart
    firestore.put('/session/{}/{}'.format(session_id, product), 'quantity', quantity)

# # Function to create a new cart in case the cart is empty
# def createNewSession(product, metaData):
#     global session_id
#     data = {product: metaData}
#     uid = firestore.post('/session/', data)
#     session_id = uid['name']
#     firestore.put('/sessionIDs/', 'dev', session_id)

# Function to delete an item
def deleteEntry(session_id, product):
    firestore.delete('/session/{}/'.format(session_id), product)

# Function to add item to cart
def add_to_cart(item):
    cart = firestore.get('/session/{}/'.format(session_id), '')
    # if item is recognized and not Null
    if item != 'Null':
        # If the cart exists already
        try:
            # if recognized item is already in cart then increase quantity
            if item in list(cart.keys()):
                quantity = cart[item]['quantity'] + 1
                updateQuantity(session_id, item, quantity)
                print('Increased Quantity by 1')

            # if recognized item is NOT already in cart then add it
            else:
                addToRTDB(session_id, item, generateMeta(item, 1))
                print('Added 1 item to Cart')

        # If the cart doesn't exist then a new session is created
        except:
            print('Creating a new session')
            createNewSession(item, generateMeta(item, 1))

# Function to remove item from cart
def del_from_cart(item):
    cart = firestore.get('/session/{}/'.format(session_id), '')
    # if item is recognized and not Null
    if item != 'Null':
        # If the cart exists already
        try:
            # if recognized item is already in cart then decrease quantity
            if item in list(cart.keys()):
                quantity = cart[item]['quantity']
                if quantity > 1:
                    quantity -= 1
                    updateQuantity(session_id, item, quantity)
                    print('Decreased Quantity by 1')
                else:
                    deleteEntry(session_id, item)
                    print('Deleted Item from Cart')

            # if recognized item is NOT in cart
            else:
                print('Item Not in Cart to be removed')

        # If the cart doesn't exist then a new session is created
        except:
            print("Cart Doesn't Exists")


# Route http posts to this method
def hello_world(request):
    if request.method == 'POST':
        # Receive the action to be performed (ADD | DEL)
        action = json.load(request.files['json'])['action']
        # Receive the image file
        file = request.files['file']
        file.save('/tmp/tmp.jpg')
        # Perform predictions
        output, confidence = infer('/tmp/tmp.jpg')
        print(output, confidence)

        if action == 'ADD':
            # Add product to CART
            add_to_cart(output)
        elif action == 'DEL':
            # Remove product from CART
            del_from_cart(output)

        # Respond with the name of the product from the product_map dictionary
        return str(output), 200
    else:
        return 'OK', 200
