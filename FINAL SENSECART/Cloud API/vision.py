from google.cloud import vision

CREDS = 'gcreds.json'
PROJECT_ID = 'lucid-messenger-251717'
LOCATION = 'us-east1'

def vps_predict(product_set_id, file_path, product_category='packagedgoods-v1', path=True, analyse=False):
    """Search similar products to image.
    Args:
        project_id: Id of the project.
        location: A compute region name.
        product_set_id: Id of the product set.
        product_category: Category of the product.
        file_path: Local file path of the image to be searched.
        path: if False, then file_path is assumed to be stream of bytes
        analyse: if True, the full results is printed. This is useful for debugging and checking performance

    Returns:
        product_name (str)
        confidence (float)
    """
    # product_search_client is needed only for its helper methods.
    product_search_client = vision.ProductSearchClient.from_service_account_json(CREDS)
    image_annotator_client = vision.ImageAnnotatorClient.from_service_account_json(CREDS)

    # Read the image as a stream of bytes.
    if path:
        with open(file_path, 'rb') as image_file:
            content = image_file.read()
    else:
        content = file_path

    # Create annotate image request along with product search feature.
    image = vision.types.Image(content=content)

    # product search specific parameters
    product_set_path = product_search_client.product_set_path(
        project=PROJECT_ID, location=LOCATION,
        product_set=product_set_id)
    product_search_params = vision.types.ProductSearchParams(
        product_set=product_set_path,
        product_categories=[product_category])
    image_context = vision.types.ImageContext(
        product_search_params=product_search_params)

    # Search products similar to the image.
    response = image_annotator_client.product_search(
        image, image_context=image_context)

    index_time = response.product_search_results.index_time
#     print('Product set index time:')
#     print('  seconds: {}'.format(index_time.seconds))
#     print('  nanos: {}\n'.format(index_time.nanos))

    results = response.product_search_results.results

    if analyse is True:
        print('Full Search results:')
        for result in results:
            product = result.product

            print('Score(Confidence): {}'.format(result.score))
            print('Image name: {}'.format(result.image))

            print('Product name: {}'.format(product.name))
            print('Product display name: {}'.format(
                product.display_name))
            print('Product description: {}\n'.format(product.description))
            print('Product labels: {}\n'.format(product.product_labels))

    try:
        product = results[0].product
        confidence = results[0].score
        product_id = product.name
        product_name = product.display_name

        print('Predicted: {}\nConfidence: {}\nProduct ID: {}'.format(product_name, round(confidence*100, 2), product_id))
        return product_name, confidence

    except:
        print('Something Went Wrong')
        return 'Null', 0
