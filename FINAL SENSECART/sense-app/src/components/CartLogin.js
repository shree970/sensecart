import React from "react";

class CartLogin extends React.Component {
  state = { id: "" };

  onInputChange = event => {
    this.setState({ id: event.target.value });
  };

  onLogin = () => {
    this.props.onLogin(this.state.id);
  };
  render() {
    return (
      <div className="ui card">
        <div className="content">
          <div className="header">Enter Cart ID</div>

          <div className="description">
            <div className="ui input">
              <input
                type="text"
                placeholder="Enter ID"
                onChange={this.onInputChange}
              />
            </div>
          </div>
        </div>

        <div className="ui icon buttons" wfd-id="323">
          <div
            className="ui bottom attached teal green button"
            onClick={this.onLogin}
          >
            Login
          </div>
        </div>
      </div>
    );
  }
}

export default CartLogin;
