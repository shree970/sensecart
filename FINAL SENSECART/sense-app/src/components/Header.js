import React from "react";
import logo from "./images/logo.png";

class Header extends React.Component {
  render() {
    return (
      <div className="inverted ui large top fixed borderless menu">
        <div className="ui container">
          <div className="item">
            <img src={logo} />
          </div>
          <a className={`${this.props.home} item`} href="/">
            Home
          </a>
          <a className={`${this.props.dash} item`} href="dash">
            Dashboard
          </a>
          <div className="right menu">
            <div className="item">
              <a className="ui button" href="dash">
                Log in
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
