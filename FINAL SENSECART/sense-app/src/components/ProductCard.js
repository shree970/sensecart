import React from "react";

class ProductCard extends React.Component {
  removeBtn = () => {
    this.props.removeItem(this.props.id);
  };
  render() {
    return (
      <div className="ui card">
        <div className="content">
          <div className="header">{this.props.product}</div>
          <div className="description">Quantity: {this.props.quantity}</div>
          <div className="description">
            Price of one: Rs. {this.props.pricetag} /-
          </div>
        </div>
      </div>
    );
  }
}

export default ProductCard;
