import firebase from "firebase";
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBUgYXOHMgQWzb5RhOxOvKntBQsK1a58t4",
  authDomain: "sensecart-6ece7.firebaseapp.com",
  databaseURL: "https://sensecart-6ece7.firebaseio.com",
  projectId: "sensecart-6ece7",
  storageBucket: "sensecart-6ece7.appspot.com",
  messagingSenderId: "554219222580",
  appId: "1:554219222580:web:5989a13e3574d351c2fc61",
  measurementId: "G-0EH8W7H7YD"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export default firebase;
