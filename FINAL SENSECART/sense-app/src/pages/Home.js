import React from "react";
import ProductCard from "../components/ProductCard";
import Header from "../components/Header";
import sensecart from "../components/images/cart.png";

import firebase from "../firebase";

class Home extends React.Component {
  state = {};

  render() {
    return (
      <div>
        <Header home="active" />
        <div
          className="ui inverted vertical center aligned segment"
          style={{ height: "754px" }}
        >
          <div className="ui text container" style={{ marginTop: "150px" }}>
            <h1 className="ui horizontal divider inverted header">SenseCart</h1>
            <h2>Cashierless Shopping Cart</h2>
            <a
              className="ui huge primary button"
              href="dash"
              style={{ marginTop: "10px" }}
            >
              Get Started <i className="right arrow icon"></i>
            </a>
            <img className="ui centered large image" src={sensecart} />
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
