import React, { Component } from "react";
import QrReader from "react-qr-scanner";
import Header from "../components/Header";
import Dashboard from "./Dashboard";

class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      delay: 100,
      result: "No result"
    };

    this.handleScan = this.handleScan.bind(this);
  }
  handleScan(data) {
    this.setState({
      result: data
    });
    let code = "SC";
    if (data != null) {
      if (data.includes(code)) {
        this.props.onLogin(data);
      }
    }
  }
  handleError(err) {
    console.error(err);
  }
  render() {
    const previewStyle = {
      height: 240,
      width: 320,
      objectFit: ""
    };

    return (
      <div>
        <Header />
        <div className="ui centered container aligned segment">
          <center>
            <QrReader
              delay={this.state.delay}
              style={previewStyle}
              onError={this.handleError}
              onScan={this.handleScan}
              facingMode="rear"
            />
            <h2> Scan the QR Code on SenseCart to Start </h2>
          </center>
        </div>
      </div>
    );
  }
}
export default Auth;
