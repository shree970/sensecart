import React from "react";
import Cart from "./Cart";
import Auth from "./Auth";
import Header from "../components/Header";
import CartLogin from "../components/CartLogin";
import firebase from "../firebase";
import { Redirect } from "react-router-dom";

class Dashboard extends React.Component {
  state = {
    id: "",
    displayCart: false,
    displayScanner: false
  };
  componentDidMount() {
    const cart = firebase.database().ref("/sessionIDs/" + this.state.id);
    cart.on("value", snapshot => {
      let skey = snapshot.val();
      this.setState({ key: skey, displayCart: true });
      console.log(skey);

      this.setState({ id: this.props.id });
      console.log(this.state.id);
    });
  }

  onLogin = id => {
    console.log(id);
    this.setState({ id: id });
    firebase
      .database()
      .ref("/session/" + id + "/Looks Empty")
      .set({
        init: "init"
      });
    console.log("Logged In");
  };

  onScan = () => {
    this.setState({ displayScanner: !this.state.displayScanner });
  };

  render() {
    if (this.state.id === undefined) {
      return (
        <div className="ui container">
          <Header />
          <div style={{ marginTop: "70px" }}>
            <center>
              <CartLogin onLogin={this.onLogin} />
              <h3 className="ui horizontal divider header">OR</h3>
              <button className="ui button teal" onClick={this.onScan}>
                Scan QR Code on Cart
              </button>
              {this.state.displayScanner && <Auth onLogin={this.onLogin} />}
            </center>
          </div>
        </div>
      );
    }

    return (
      <div className="ui container">
        <Header dash="active" />
        <div style={{ marginTop: "70px" }}>
          {this.state.displayCart && <Cart sess={this.state.id} />}
        </div>
      </div>
    );
  }
}

export default Dashboard;
