import React from "react";
import ProductCard from "../components/ProductCard";
import Header from "../components/Header";
import firebase from "../firebase";
import { Redirect, Route } from "react-router-dom";
import Payment from "./Payment";

class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cartlist: [],
      firebase: {},
      session_id: this.props.sess,
      session: true,
      checkout: false,
      bill: 0
    };
  }

  componentDidMount() {
    // Getting cart data like name, quantity
    const cart = firebase.database().ref("/session/" + this.state.session_id);
    cart.on("value", snapshot => {
      let products = snapshot.val();
      let newState = {};
      let newIds = [];
      for (let product in products) {
        newState[product] = {
          price: products[product].price,
          quantity: products[product].quantity
        };
        newIds.push(product);
      }
      this.setState({ firebase: newState, cartlist: newIds });
      if (newIds.length > 1) {
        this.removeItem("Looks Empty");
        //console.log("Emptying");
      } else if (newIds.length == 0 && this.state.session === false) {
        firebase
          .database()
          .ref("/session/" + this.state.session_id + "/Looks Empty")
          .set({
            init: "init"
          });
      }
    });

    console.log(this.state.cartlist);
  }

  getItemTotal = item => {
    var quant = this.state.firebase[item].quantity;
    var price = this.state.firebase[item].price;
    var item_bill = parseFloat(quant) * parseFloat(price);
    return item_bill;
  };

  getBillTotal = items => {
    let bill = 0;
    console.log(items);
    for (var i = 0; i < items.length; i++) {
      var price = this.getItemTotal(items[i]);
      bill += price;
    }

    console.log("Bill: " + bill);
    this.setState({ bill: bill });
    console.log("State Bill: " + this.state.bill);
  };

  Checkout = () => {
    this.getBillTotal(this.state.cartlist);
    this.setState({ checkout: true });
  };

  removeItem = item => {
    this.setState(state => {
      var index = state.cartlist.indexOf(item);
      const cartlist = state.cartlist;
      if (index > -1) {
        cartlist.splice(index, 1);
      }
      return {
        cartlist
      };
    });
    // console.log(this.state.firebase);
    // console.log(this.state.cartlist);
    firebase
      .database()
      .ref("/session/" + this.state.session_id + "/" + item)
      .remove();
  };

  endSession = () => {
    firebase
      .database()
      .ref("/session/" + this.state.session_id)
      .remove();
    this.setState({ session: false });
  };

  addItem = item => {
    var quant = this.state.firebase[item].quantity;
    var price = this.state.firebase[item].price;
    //console.log(item);

    if (item == "Looks Empty") {
      return (
        <div
          className="ui four wide computer sixteen wide mobile column"
          key="0"
        >
          <div className="ui card">
            <div className="content">
              <div className="header">Looks Empty</div>
              <div className="description">
                Swing a Product Across the device to Start
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div
          className="ui four wide computer sixteen wide mobile column"
          key={item}
        >
          <ProductCard
            id={item}
            product={item}
            pricetag={price}
            quantity={quant}
          />
        </div>
      );
    }
  };

  addItems = items => {
    //console.log(items);
    return items.map(this.addItem);
  };
  render() {
    if (this.state.session === false) {
      return <Redirect to="/" />;
    } else if (this.state.checkout === true && this.state.bill > 0) {
      return (
        <Route
          render={props => (
            <Payment
              {...props}
              bill={this.state.bill}
              sess={this.state.session_id}
            />
          )}
        />
      );
    }
    return (
      <div className="ui segment">
        <div className="ui grid">
          <div className="ui sixteen wide mobile eight wide computer column">
            <h1> Using Cart No. {this.props.sess} </h1>
            <p> Use {this.props.sess} to Log Back In in case you log out</p>
          </div>
          <div className="ui sixteen wide mobile eight wide computer column">
            <button
              className="ui left floated right labeled icon teal button"
              onClick={this.Checkout}
            >
              Checkout <i className="right arrow icon"></i>
            </button>
            <button
              className="ui right floated right basic red button"
              onClick={this.endSession}
            >
              Clear Session
            </button>
          </div>
          {this.addItems(this.state.cartlist)}
        </div>
      </div>
    );
  }
}

export default Cart;
