import React, { Component } from "react";
import Header from "../components/Header";
import { Redirect, Route } from "react-router-dom";
import firebase from "../firebase";

class Payment extends Component {
  state = { paid: false };

  onPay = () => {
    firebase
      .database()
      .ref("/session/" + this.props.sess)
      .remove();
    this.setState({ paid: true });
    console.log("Paid");
  };

  render() {
    if (this.state.paid === true) {
      return <Redirect to="/end" />;
    }
    return (
      <div>
        <Header />
        <div className="ui container">
          <div className="ui segment" style={{ marginTop: "70px" }}>
            <h2>Cart No. {this.props.sess}</h2>
            <h3>Total Payable Bill: {this.props.bill}</h3>
            <button className="ui primary button" onClick={this.onPay}>
              Pay Now
            </button>
          </div>
        </div>
      </div>
    );
  }
}
export default Payment;
