import React, { Component } from "react";
import Header from "../components/Header";
import { Redirect, Route } from "react-router-dom";

class End extends Component {
  render() {
    return (
      <div>
        <Header />
        <div
          className="ui vertical center aligned segment"
          style={{ height: "754px" }}
        >
          <div className="ui text container" style={{ marginTop: "150px" }}>
            <h2>Thank You for Shopping with Us</h2>
            <a
              className="ui right labeled icon primary button"
              href="/"
              style={{ marginTop: "10px" }}
            >
              Go Back to Home <i className="home icon"></i>
            </a>
          </div>
        </div>
      </div>
    );
  }
}
export default End;
