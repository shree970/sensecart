import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Home from "./pages/Home";
import Cart from "./pages/Cart";
import Auth from "./pages/Auth";
import End from "./pages/End";
import Payment from "./pages/Payment";
import Dashboard from "./pages/Dashboard";

class App extends React.Component {
  state = { cartID: "" };

  onLogin = cartID => {
    this.setState({ cartID: cartID });
  };
  render() {
    return (
      <BrowserRouter>
        <div>
          <Route
            exact={true}
            path="/"
            render={() => (
              <div className="App">
                <Home />
              </div>
            )}
          />
          <Route
            exact={true}
            path="/dash"
            render={() => (
              <div className="App">
                <Dashboard />
              </div>
            )}
          />
          <Route
            exact={true}
            path="/end"
            render={() => (
              <div className="End">
                <End />
              </div>
            )}
          />
          <Route
            exact={true}
            path="/payment"
            render={() => (
              <div className="Payment">
                <Payment />
              </div>
            )}
          />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
