# Sensecart

### Cashierless Shopping Cart

This directory contains all final code used to build Sensecart.

## Inference API

Go to [Cloud API](Cloud API) folder for details

## Vision API

[Vision API](Vision API) contains the following

1. [Notebook](Vision API/Vision API Product Search.ipynb) with the functions to upload a dataset for Visual Product Search and also running inference on some input image.
2. [ref-images](Vision API/ref-images) contains the dataset for Vision API which was uploaded to Google Cloud Storage

Note: In the notebook's Inference section you will find the `PRODUCT_SET_ID` as `snacks00` which contains different dataset than the ones in [ref-images](Vision API/ref-images).

- `snacks00` dataset contains Lays Blue, Lays Green, Lays Limon and Aloo Bhujia. This dataset is the current deployed version.
- `demo0` is the dataset which contains the images in ref-images folder. This dataset is currently in operation with Vision API and can take upto 8 hours for setup since deploment. This dataset will probably be ready to use by 10:00 AM 5-Feb-2020. Once it gets deployed, the Inference API's Vision Module will be updated with the new dataset i.e. `demo0`.

## Pi Client

Head over to [pi_client.py](pi_client.py) for the script running on pi.

Functionality of the Device:

1. To Add a product, the user can show the product to the device by bringing it in front from the right side
2. To Remove a product, the user can show the product to the device by bringing it in front from the left side

These product movement gestures can be configured for up and down too.

## sense-app

[sense-app](sense-app) contains the code for the react app as well as the static build version which can be run easily with node installed.

To run the application locally, run the following commands in terminal.

1. Change directory to `sense-app`
2. Run `npm install -g serve`
3. Then you can start the static server by running `serve -s build`

If you want to test the app on mobile, there are a few additional steps.

4. Install ngrok and open another terminal
5. Then port the local host to a url with https by running `ngrok http 5000` in terminal
6. It will give an output similar to this
   ![ngrok](ngrok.png)
7. Copy the url with `https` and run it on your mobile device.

Note: The camera access won't work if the `http` url is used. So use `https` only.

Common Issues with ngrok: You might get an error screen saying `Too many connections`. If that comes just wait for a minute and refresh the page.

#### App Usage

This App is a mobile first-app so the UI is designed with mobile in mind. So its best if you use mobile to test it or you can just resize the browser window to mobile size and ratios.

1. Home Page

   ![homepage](homepage.png)

2. To start shopping you can either click on Log In or Get Started. This will route you to the login page. The user can either manually type the code on the cart or use the QR code scanner to scan the cart, in order to start the shopping session. For Testing purposes you can use the code SC001 or SC002 as it already has some products in the database.

   ![login](login.png)

3. The dashboard shows your products. You can click on the Clear Session button to erase the session if you want to start over. As you add or remove products from the shopping cart, the dashboard updates itself in realtime.

   ![dashboard](dashboard.png)

4. Click on the checkout button to head over the payment page where it shows you the total bill. You can click on Pay Now button from there. (Pay Now is not functional with a payment gateway)

   ![checkout](checkout.png)

5. Clicking on Pay Now will lead you to the Thank you page with a button to go back to home.

   ![end](end.png)
