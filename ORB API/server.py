from flask import Flask, request, jsonify
import numpy as np
from bruteORB import ORBMatcher
import cv2

# Initialize the Flask application
app = Flask(__name__)

# Define the index to product mapping
product_map = {0: 'Fevicol', 1: 'Tablets', 2: 'Makhana'}

# Initialize the ORBMatcher and load the DB
cvmodel = ORBMatcher()
DB = cvmodel.load_db('pidb')

def infer(image):
    distances, pred_idx = cvmodel.predict(image, DB)
    if distances[pred_idx] < 35:
        return product_map[pred_idx], distances
    else:
        return 'Nothing Detected', distances

# Cart List
CART = []

def add_cart(product):
    global CART
    CART.append(product)

def get_cart():
    global CART
    return CART

# Route http posts to this method
@app.route('/dev', methods=['POST', 'GET'])
def test():
    if request.method == 'POST':
        # Convert string of image data to uint8
        nparr = np.frombuffer(request.data, np.uint8)
        # Decode image
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
        # Perform predictions
        output, distances = infer(img)
        # Add product to CART
        add_cart(output)
        # Respond with the name of the product from the product_map dictionary

        return str(output), 200
    else:
        # The GET method is still incomplete and will be
        # used for updating dashboards and getting historical
        # results from the server
        cart = get_cart()
        return 'CART: {}'.format(cart), 200


# start flask app
app.run(port=5002)
