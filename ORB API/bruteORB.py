import numpy as np
from statistics import mean
import cv2 as cv
import os
from time import time

class ORBMatcher():

    def __init__(self):
        # Initiate ORB detector
        self.orb = cv.ORB_create()
        # create BFMatcher object
        self.bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)

    def match_single(self, img1, img2):
        # find the keypoints and descriptors with ORB
        kp1, feat1 = self.orb.detectAndCompute(img1,None)
        kp2, feat2 = self.orb.detectAndCompute(img2,None)

        # Match descriptors.
        matches = self.bf.match(feat1,feat2)
        # Sort them in the order of their distance.
        matches = sorted(matches, key = lambda x:x.distance)
        try:
            distance = mean([matches[i].distance for i in range(10)])
            return distance
        except:
            return 100

    def load_db(self, db_path):
        '''Load and preprocess the database images'''
        db = []

        for ref in os.listdir(db_path):
            img = cv.imread(os.path.join(db_path, ref))
            db.append(img)

        return db

    def predict(self, image, db):
        distances = []

        for ref in db:
            distances.append(self.match_single(ref, image))

        return distances, np.argmin(distances)

# cvmodel = ORBMatcher()
# DB = cvmodel.load_db('pidb')
# image = cv.imread('queryImage.jpg')
# distances, pred_idx = cvmodel.predict(image, DB)
# print(pred_idx, distances[pred_idx])
