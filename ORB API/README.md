# Brute-Force Matching with ORB for Product Recognition

## `bruteORB.py`

This module contains high level functions for using the ORB algorithm in the product database.

#### Example Usage

Run the following example code snippet to test out the model on [queryImage.jpg](queryImage.jpg)

```python
from bruteORB import ORBMatcher
# Initialize the ORBMatcher
cvmodel = ORBMatcher()
# Load the database folder containing reference images
# against which the query image will be matched
DB = cvmodel.load_db('pidb')
# Read the query image
image = cv.imread('queryImage.jpg')
# Make the predictions
distances, pred_idx = cvmodel.predict(image, DB)
# Print the output
print(pred_idx, distances[pred_idx])
# the pred_idx output should be 1 as himalaya product
# is at index 1 in the db
```

## `server.py`

This is the Backend API script running the ORBMatcher which can be accessed by making a POST request from the client by sending an image and the API will respond with the predicted product.

The POST request to the server of an encoded image should be made to `{base_url}/dev` with the appropriate headers.

Refer [server.py](server.py) and [pi_client.py](pi_client.py) for usage.

#### Note

The matching algorithm works best when the images of the DB and the query image are taken from the same camera as resolution plays a key role. The pictures in this repo were taken from a raspberry pi camera. As the test images will be from a raspberry pi camera, the DB was also made with the same camera.

This limitation can be overcome by using an ML algorithm to get the similarity instead of Euclidean Distances by the Brute Force Matching algorithm of OpenCV.

Logistic Regression or an SVM Classifier is enough for taking the two feature descriptions by ORB and making a prediction of either 1 or 0 to learn a similarity function and then can be used in future for expanded datasets without retraining. Using an ML model instead of a DL model for this task will increase efficiency and speed as using DL will be an overkill.
