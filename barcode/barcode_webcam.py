# import the necessary packages
from pyzbar import pyzbar
import argparse
import cv2
import matplotlib.pyplot as plt

# construct the argument parser and parse the arguments
# ap = argparse.ArgumentParser()
# ap.add_argument("-i", "--image", required=True,
# 	help="path to input image")
# args = vars(ap.parse_args())

# load the input image
# image = cv2.imread(args["image"])

cam = cv2.VideoCapture('https://192.168.1.2:8080/video')
cv2.namedWindow("output", cv2.WINDOW_NORMAL)
while True:
    ret, image = cam.read()
    image = cv2.resize(image, (960, 540))
    # find the barcodes in the image and decode each of the barcodes
    barcodes = pyzbar.decode(image)
    print(barcodes)

    # loop over the detected barcodes
    for barcode in barcodes:
    	# extract the bounding box location of the barcode and draw the
    	# bounding box surrounding the barcode on the image
    	(x, y, w, h) = barcode.rect
    	cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)

    	# the barcode data is a bytes object so if we want to draw it on
    	# our output image we need to convert it to a string first
    	barcodeData = barcode.data.decode("utf-8")
    	barcodeType = barcode.type

    	# draw the barcode data and barcode type on the image
    	text = "{} ({})".format(barcodeData, barcodeType)
    	cv2.putText(image, text, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX,
    		0.5, (0, 0, 255), 2)

    	# print the barcode type and data to the terminal
    	print("[INFO] Found {} barcode: {}".format(barcodeType, barcodeData))

    # show the output image
    cv2.imshow("bar", image)
    cv2.imwrite('photo.jpg', image)
    if cv2.waitKey(25) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
cam.release()
